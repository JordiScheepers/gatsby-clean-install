module.exports = {
    siteMetadata: {
        siteUrl: `https://www.yourdomain.tld`,
    },
    plugins: [
        `gatsby-plugin-typescript`,
        /*
          Plugin voor import { ReactComponent as <var> } from 'path-to-file/file-name.svg';
          Werkt toch niet, verkeerd onthouden gok ik. 
          Deze zit in het website project, dus deze wil ik wel werkend krijgen
        */
        // `gatsby-plugin-svgr`,

        /*
          Zit niet in het website project, maar werkt wel
        */
        {
            resolve: "gatsby-plugin-react-svg",
            options: {
              rule: {
                include: /images/
              }
            }
          }
    ]
}