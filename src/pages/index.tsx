import React from 'react';
import styled from 'styled-components';
import Logo from '../images/logo.svg';
/*
  SVGR gatsby plugin import
*/
// import { ReactComponent as Logo } from '../images/logo.svg';

const StyledLogo = styled(Logo);

const StyledContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 1em;
`;

const Index: React.FC = () => {
  return (
    <StyledContainer>
      <p>Kijk dit:</p>
      <Logo />
    </StyledContainer>
  )
}

export default Index;