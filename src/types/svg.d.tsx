declare module "*.svg" {
  const content: any;
  export const ReactComponent: React.ReactElement;
  export default content;
}